import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
import * as _ from 'underscore';
import * as Clappr from 'clappr';
@Component({
  selector: 'app-auditorium-two',
  templateUrl: './auditorium-two.component.html',
  styleUrls: ['../auditorimStl/audi.style.scss']
})
export class AuditoriumTwoComponent implements OnInit , OnDestroy {
  textMessage = new FormControl('');
  newMessage: string[] = [];
  msgs: string;
  messageList: any = [];
  roomName = 'hallB';
  serdia_room = localStorage.getItem('serdia_room');
  datas:any;
  player:any
  actives: any = [];
  vidURL2:any
  newWidth;
  newHeight;
  like = false;
  videoPlayer:any
  company = "MultiTV";
  videoEnd = false;
  // videoPlayerTwo = 'https://dhzjgfv9krlhb.cloudfront.net/abr/smil:stream1.smil/playlist.m3u8';
  // videoPlayer= 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session2.smil/playlist.m3u8';
  interval;
  

  constructor(private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.audiActive();
    this.interval= setInterval(() => {
      this.getHeartbeat(); 
         }, 60000);
  //  this.playVideo();

    // console.log(this.videoPlayer, 'this is 1');
    this.loadData();
    this.chatService.getconnect('toujeo-153');
    this.chatService.getMessages().subscribe((data => {
      //  console.log('data',data);
      if (data == 'groupchat') {
        this.chatGroupTwo();
      }

    }));
  }
  audiActive() {
    this._fd.activeAudi('216').subscribe(res => {
      this.actives = res.result;
      this.videoPlayer = res.result;
      this.vidURL2 = res.result[0].stream
      console.log(this.vidURL2);
      this.playVideo();
    })

    // this._fd.activeAudi().subscribe(res => {
    //   this.actives = res.result;
    //   this.videoPlayer = res.result;
    //   this.vidURL2 = this.actives[0].stream
    //   console.log(this.vidURL2);
    //   this.playVideo()
    // })
  }
  playVideo() {
    var playerElement = document.getElementById("player_sec");
    this.player = new Clappr.Player({
      parentId: 'player_sec',
      source: this.vidURL2,
      // source: "http://d3tn0h9cityxqm.cloudfront.net/streamline/chunks/701_5ef2e2fc9223a/701_5ef2e2fc9223a_master.m3u8",
      poster: 'assets/posterb.jpg',
      height: '90%',
      maxBufferLength: 30,
      width: '100%',
      autoPlay: true,
      loop: true,
      hideMediaControl: false,
      hideVolumeBar: false,
      hideSeekBar: false,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);
    // $('#player-wrapper > div > .media-control').css({ 'height': '0'});
    // if (window.innerWidth <= 572) {
    //   this.player.play();
    //   this.player.resize({ width: '100%', height: window.innerWidth / 2 + 30 });
    // } else {
    //   this.player.play();
    //   var aspectRatio = 9 / 16;
    //   // tslint:disable-next-line: prefer-const
    //   this.newWidth = document.getElementById('player-wrapper').parentElement
    //     .offsetWidth;
    //   this.newHeight = 2 * Math.round((this.newWidth * aspectRatio) / 2);
    //   if (this.newWidth < 1000) {
    //   this.player.resize({ width: this.newWidth, height: this.newHeight });
    //   $('.container')
    //     .find('.player-poster')
    //     .css('background-size', 'contain');
    // }
    // }
  }
  getHeartbeat(){
    let data = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('user_id', data.id );
    formData.append('event_id', '153');
    formData.append('audi', '216');
    this._fd.heartbeat(formData).subscribe(res=>{
      console.log(res);
    })
  }
  videoEnded() {
    this.videoEnd = true;
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  playAudioWhistle() {
    let playaudio: any = document.getElementById('myAudioWhistle');
    playaudio.play();
  }
  openGroupChat() {
    $('.groupchatTwo').modal('show')
    this.loadData();
  }
  likeopen(){
    this.like = true;
    setTimeout(() => {
      this.like = false;
    }, 10000);
  }
  
  loadData(): void {
    this.chatGroupTwo();
    this.chatService.getconnect('toujeo-153');
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.chatService.addUser(data.name, this.serdia_room);
    localStorage.setItem('username', data.name);
    this.chatService
      .receiveMessages(this.serdia_room)
      .subscribe((msgs: any) => {
        if (msgs.roomId === 2){
          this.messageList.push(msgs);
        }        
        console.log('demo', this.messageList);
      });
  }
  chatGroupTwo() {
    this._fd.groupchating2().subscribe(res => {
      console.log('res', res);
      this.messageList = res.result;
      // $('.groupchatsModal').modal('toggle');
      // $('.groupchatsModal').trigger('click');

    });
  }
  closePopup() {
    $('.groupchatTwo').modal('hide');
  }
  // postMessageTwo(value) {
    
  //   let data = JSON.parse(localStorage.getItem('virtual'));
    
  //   this.chatService.sendMessage(value, data.name, this.serdia_room);
    
  //   this.textMessage.reset();
  
  // }
  postMessageTwo(value) {

    let data = JSON.parse(localStorage.getItem('virtual'));
    this.datas = data.id;
    this.datas = data.name;

    const formData = new FormData();
    formData.append('room_name', this.roomName);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('event_id', '153');
    formData.append('created', '2021-03-17 18:18:18');

    this._fd.postGroup(formData).subscribe(res => {
      console.log('res', res);
      let arr = {
        'user_id': data.id,
        'user_name': data.name,
        'room_name':this.roomName,
        'email':data.email,
        'created': '2021-03-17 18:18:18',
        'chat_data':value,
        'is_approved': '1'
      };
      this.messageList.push(arr);
      // this.messageList = res.result;
    });
    this.textMessage.reset();
    

  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let hrs: any = new Date().getHours();
    let mns: any = new Date().getMinutes();
    let secs: any = new Date().getSeconds();

    // alert(time)
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (hrs < 10) {
      hrs = '0' + hrs;
    }
    if (mns < 10) {
      mns = '0' + mns;
    }
    if (secs < 10) {
      secs = '0' + secs;
    }
    const formData = new FormData();
    formData.append('event_id', '153');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'others');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + hrs+':'+mns+':'+secs);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  ngOnDestroy(){
    clearInterval(this.interval);
  }
}
