import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumfourComponent } from './auditoriumfour.component';


const routes: Routes = [
  {path:'', component:AuditoriumfourComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumFourRoutingModule { }
