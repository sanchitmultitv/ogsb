import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeLobbyRoutingModule } from './welcome-lobby-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WelcomeLobbyRoutingModule
  ]
})
export class WelcomeLobbyModule { }
