import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-welcome-lobby',
  templateUrl: './welcome-lobby.component.html',
  styleUrls: ['./welcome-lobby.component.scss']
})
export class WelcomeLobbyComponent implements OnInit {
  showPoster=false;
  constructor() { }

  ngOnInit(): void {
  }
  endPlayscreen(){
    this.showPoster = true;
  }
  playScreenOnImg(){
    this.showPoster = false;
    let playVideo: any = document.getElementById("playVideo");
    playVideo.play();  
  }
  openpopup(){
    if (localStorage.getItem('whatever')) {
      $('.NoFeedback').modal('show');
    }else{
      $('.feebackModal').modal('show');
    }
  }
  closePopup() {
    $('.NoFeedback').modal('hide');
}
}