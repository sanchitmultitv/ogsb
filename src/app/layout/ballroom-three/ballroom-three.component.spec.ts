import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BallroomThreeComponent } from './ballroom-three.component';

describe('BallroomThreeComponent', () => {
  let component: BallroomThreeComponent;
  let fixture: ComponentFixture<BallroomThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BallroomThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BallroomThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
