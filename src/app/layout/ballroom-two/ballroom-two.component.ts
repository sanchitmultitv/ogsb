import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-ballroom-two',
  templateUrl: './ballroom-two.component.html',
  styleUrls: ['./ballroom-two.component.scss']
})
export class BallroomTwoComponent implements OnInit,OnDestroy {

  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.getQA();
  //   this.interval = setInterval(() => {
  //    this.getQA();
  //  }, 100000);

 }
 closePopup() {
   $('.ballroomQuestionModalTwo').modal('hide');
 }
 getQA(){
   let data = JSON.parse(localStorage.getItem('virtual'));
  //  this._fd.Liveanswers().subscribe((res=>{
  //    console.log(res);
  //    this.qaList = res.result;
  //  }))
 }
 postQuestion(value){
   let data = JSON.parse(localStorage.getItem('virtual'));
   let audi_id = '216'
  console.log(value, data.id, audi_id);
   this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
     //console.log(res);
     if(res.code == 1){
       this.msg = 'Submitted Succesfully';
     }
     this.textMessage.reset();
     $('.ballroomQuestionModalTwo').modal('toggle');

   }))
 }
 ngOnDestroy() {
   clearInterval(this.interval);
 }

}
